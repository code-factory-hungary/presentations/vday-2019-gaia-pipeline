#### Run minikube on virtualbox, and set memory
```shell script
minikube start
```

#### Run hasicorp vault 
```shell script
docker run --cap-add=IPC_LOCK -d \
    -e 'VAULT_DEV_ROOT_TOKEN_ID=root-token' \
    -e 'VAULT_ADDR=http://localhost:8200' \
    -e 'VAULT_TOKEN=root-token' \
    -p 8200:8200 --name=vault vault:latest
```
### Hashicorp vault setup
```shell script
    docker cp ~/.kube/config vault:/tmp/config
    docker exec -it vault sh
    vault kv put secret/kube-conf conf="$(cat /tmp/config | base64)"

```

#### Gaia setup
Change Makefile default dev command to dev=false
```shell script
    make compile_frontend
```

#### Run Gaia Pipeline
```shell script
    make
```